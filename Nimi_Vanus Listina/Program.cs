﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimi_Vanus_Listina
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> õpilasteNimekiri = new Dictionary<string, int>();
            while(true)
            {
                Console.Write("Sisesta õpilase nimi: ");
                string nimi = Console.ReadLine(); // ReadLine ootab enterit, loeb rea lõpuni; saab ka ReadKey() - klahvivajutus
                if (nimi != "")
                { 
                    Console.Write("Sisesta õpilase vanus: ");
                    int vanus = int.Parse(Console.ReadLine());
                    õpilasteNimekiri.Add(nimi, vanus);
                } else break;

            }
            foreach (var x in õpilasteNimekiri) Console.WriteLine(x); //väljastab kõik kollektsiooni 
            Console.WriteLine(õpilasteNimekiri.Count); // palju on õpilasi

            // keskmine õpilaste vanus: dictionarys ei ole kohta vaid kasutad foreach

            double keskmineVanus = õpilasteNimekiri.Values.Average();

            Console.WriteLine(keskmineVanus); // ka Console.WriteLine("Keskmine vanus {0}", õpilasteNimikiri.Values.Averages());

            // vanim õpilane:

            double maxVanus = õpilasteNimekiri.Values.Max();
            Console.WriteLine(maxVanus);

            // vanim õpilane 2:

            foreach(var x in õpilasteNimekiri) // vari asemel võib olla KeyValuePair<string,int>
                if(x.Value == maxVanus)
                    Console.WriteLine($"Vanim on {x.Key} ta on {x.Value} aastane");

            // rohkem keskmine õpilane on:
        
            
            

        }
    }
}
